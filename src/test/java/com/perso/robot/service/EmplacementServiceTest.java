package com.perso.robot.service;

import com.perso.robot.model.Emplacement;
import com.perso.robot.model.Produit;
import com.perso.robot.repository.EmplacementRepository;
import com.perso.robot.service.impl.EmplacementServiceImpl;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class EmplacementServiceTest {

    @Mock
    private EmplacementRepository emplacementRepository;
    private AutoCloseable closeable;
    @InjectMocks
    private EmplacementServiceImpl emplacementService;

    @BeforeEach
    void initService() {
        closeable = MockitoAnnotations.openMocks(this);
    }

    @AfterEach
    void closeService() throws Exception {
        closeable.close();
    }

    private Emplacement buildEmplacement() {
        Emplacement emplacement = new Emplacement();
        emplacement.setId(1L);
        emplacement.setLargeur(4);
        emplacement.setHauteur(3);
        emplacement.setLongueur(2);
        List<Produit> produits = new ArrayList<>();
        produits.add(buildProduit());
        emplacement.setProduits(produits);
        return emplacement;
    }

    private Produit buildProduit() {
        Produit produit = new Produit();
        produit.setId(4L);
        produit.setNom("nom");
        produit.setDescription("description");
        return produit;
    }

    @Test
    public void test_getEmplacement_should_return_emplacement_with_one_produit() {
        Mockito.doReturn(Optional.of(buildEmplacement())).when(emplacementRepository).findById(Mockito.any());
        var result = this.emplacementService.getEmplacement(1L);
        assertEquals(result, buildEmplacement());
    }

}
