package com.perso.robot.model;

import jakarta.persistence.*;
import lombok.Data;

import java.util.List;

@Data
@Entity
public class Produit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String nom;

    private String description;

    @ManyToMany(mappedBy = "produits")
    private List<Commande> commandes;

    @ManyToMany(mappedBy = "produits")
    private List<Emplacement> emplacement;

}
