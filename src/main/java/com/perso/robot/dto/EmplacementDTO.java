package com.perso.robot.dto;

import lombok.Data;

import java.util.List;

@Data
public class EmplacementDTO {

    private Long id;
    private int largeur;
    private int longueur;
    private int hauteur;
    private List<ProduitDTO> produits;
}
