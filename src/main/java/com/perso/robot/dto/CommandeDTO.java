package com.perso.robot.dto;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class CommandeDTO {

    private Long id;
    private Date dateCommande;
    private List<ProduitDTO> produits;
}
