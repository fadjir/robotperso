package com.perso.robot.controller;

import com.perso.robot.dto.ProduitDTO;
import com.perso.robot.mapper.ProduitMapper;
import com.perso.robot.service.ProduitService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@RequestMapping("/produit")
public class ProduitController {

    private ProduitMapper produitMapper;
    private ProduitService produitService;

    @PostMapping("/save")
    public ProduitDTO createProduit(@RequestBody ProduitDTO produitDTO) {
        return this.produitMapper.mapToDTO(this.produitService.saveProduit(this.produitMapper.mapToEntity(produitDTO)));
    }

}
