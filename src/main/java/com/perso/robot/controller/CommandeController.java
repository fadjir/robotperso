package com.perso.robot.controller;

import com.perso.robot.dto.CommandeDTO;
import com.perso.robot.mapper.CommandeMapper;
import com.perso.robot.service.CommandeService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("commande")
public class CommandeController {

    private CommandeService commandeService;
    private CommandeMapper commandeMapper;

    @PostMapping("/save")
    public CommandeDTO createCommande(@RequestBody CommandeDTO commandeDTO) {
        return this.commandeMapper.mapToDTO(this.commandeService.saveCommande(
                this.commandeMapper.mapToEntity(commandeDTO)
        ));
    }

    @GetMapping("/{idCommande}")
    public CommandeDTO getCommande(@PathVariable Long idCommande) {
        return this.commandeMapper.mapToDTO(
                this.commandeService.findCommandeById(idCommande)
        );
    }
}
