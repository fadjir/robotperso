package com.perso.robot.controller;

import com.perso.robot.dto.EmplacementDTO;
import com.perso.robot.mapper.EmplacementMapper;
import com.perso.robot.service.EmplacementService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("emplacement")
public class EmplacementController {

    private EmplacementMapper emplacementMapper;
    private EmplacementService emplacementService;

    @PostMapping("/save")
    public EmplacementDTO createEmplacement(@RequestBody EmplacementDTO emplacementDTO) {
        return this.emplacementMapper.mapToDTO(this.emplacementService.createEmplacement(
                this.emplacementMapper.mapToEntity(emplacementDTO)
        ));
    }

    @GetMapping("/{idEmplacement}")
    public EmplacementDTO findEmplacement(@PathVariable Long idEmplacement) {
        return this.emplacementMapper.mapToDTO(this.emplacementService.getEmplacement(idEmplacement));
    }
}
