package com.perso.robot.service;

import com.perso.robot.model.Commande;

import java.util.List;

public interface CommandeService {

    Commande saveCommande(Commande commande);

    List<Commande> getAllCommande();

    Commande findCommandeById(Long id);
}
