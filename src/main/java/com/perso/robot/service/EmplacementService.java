package com.perso.robot.service;

import com.perso.robot.model.Emplacement;

import java.util.List;

public interface EmplacementService {

    Emplacement createEmplacement(Emplacement emplacement);

    Emplacement getEmplacement(Long id);

    List<Emplacement> getEmplacements();
}
