package com.perso.robot.service.impl;

import com.perso.robot.model.Emplacement;
import com.perso.robot.repository.EmplacementRepository;
import com.perso.robot.service.EmplacementService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class EmplacementServiceImpl implements EmplacementService {

    private EmplacementRepository emplacementRepository;

    @Override
    public Emplacement createEmplacement(Emplacement emplacement) {
        return this.emplacementRepository.save(emplacement);
    }

    @Override
    public Emplacement getEmplacement(Long id) {
        return this.emplacementRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Emplacement non trouvé avec cet identifiant"));
    }

    @Override
    public List<Emplacement> getEmplacements() {
        return this.emplacementRepository.findAll();
    }
}
