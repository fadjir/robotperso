package com.perso.robot.service.impl;

import com.perso.robot.model.Commande;
import com.perso.robot.repository.CommandeRepository;
import com.perso.robot.service.CommandeService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
@AllArgsConstructor
public class CommandeServiceImpl implements CommandeService {

    private CommandeRepository commandeRepository;
    @Override
    public Commande saveCommande(Commande commande) {
        if(Objects.isNull(commande.getId())) {
            commande.setDateCommande(new Date());
        }
        return this.commandeRepository.save(commande);
    }

    @Override
    public List<Commande> getAllCommande() {
        return this.commandeRepository.findAll();
    }

    @Override
    public Commande findCommandeById(Long id) {
        return this.commandeRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Pas de commande trouvée pour cet identifiant"));
    }
}
