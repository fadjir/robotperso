package com.perso.robot.service.impl;

import com.perso.robot.model.Produit;
import com.perso.robot.repository.ProduitRepository;
import com.perso.robot.service.ProduitService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class ProduitServiceImpl implements ProduitService {

    private ProduitRepository produitRepository;
    @Override
    public Produit saveProduit(Produit produit) {
        return this.produitRepository.save(produit);
    }

    @Override
    public Produit getProduit(Long id) {
        return this.produitRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Pas de produit trouvé pour cet identifiant"));
    }

    @Override
    public List<Produit> getAllProduit() {
        return this.produitRepository.findAll();
    }
}
