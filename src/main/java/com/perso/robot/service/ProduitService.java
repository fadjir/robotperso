package com.perso.robot.service;

import com.perso.robot.model.Produit;

import java.util.List;

public interface ProduitService {

    Produit saveProduit(Produit produit);

    Produit getProduit(Long id);

    List<Produit> getAllProduit();
}
