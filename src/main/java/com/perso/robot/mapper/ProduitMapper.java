package com.perso.robot.mapper;

import com.perso.robot.dto.ProduitDTO;
import com.perso.robot.model.Produit;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface ProduitMapper {

    ProduitMapper  INSTANCE = Mappers.getMapper(ProduitMapper.class);

    Produit mapToEntity(ProduitDTO produitDTO);
    ProduitDTO mapToDTO(Produit produit);

}
