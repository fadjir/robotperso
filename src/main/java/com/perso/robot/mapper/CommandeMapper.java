package com.perso.robot.mapper;

import com.perso.robot.dto.CommandeDTO;
import com.perso.robot.model.Commande;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {ProduitMapper.class})
public interface CommandeMapper {

    CommandeMapper INSTANCE = Mappers.getMapper(CommandeMapper.class);

    CommandeDTO mapToDTO(Commande commande);

    Commande mapToEntity(CommandeDTO commandeDTO);
}
