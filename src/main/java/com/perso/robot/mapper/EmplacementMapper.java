package com.perso.robot.mapper;

import com.perso.robot.dto.EmplacementDTO;
import com.perso.robot.model.Emplacement;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {ProduitMapper.class})
public interface EmplacementMapper {

    EmplacementMapper  INSTANCE = Mappers.getMapper(EmplacementMapper.class);

    Emplacement mapToEntity(EmplacementDTO emplacementDTO);
    EmplacementDTO mapToDTO(Emplacement emplacement);
}
